package com.quickspin.prototyping.game.data;

import casinoworld.games.helpers.WeightedRandomArray;

public class Model {

    //symbols
    public static final int WR = 1;
    public static final int M1 = 2;
    public static final int M2 = 3;
    public static final int M3 = 4;
    public static final int M4 = 5;
    public static final int M5 = 6;
    public static final int F6 = 7;
    public static final int F7 = 8;
    public static final int F8 = 9;
    public static final int F9 = 10;
    public static final int F10 = 11;
    public static final int BN = 12;

    // Special Symbols on Bonus Reel
    public static final int multiplierWR = 13;
    public static final int symbolSplitSymbol = 14;
    public static final int symbolSplitWR = 15;

    public static final int R4 = 15;
    public static final int R5 = 16;


    // pay out
    public static final int[][] PAY_OUT = new int[][]{
            null, //0
            {0, 0, 0, 40, 100, 400},    // WR
            {0, 0, 0, 40, 100, 400},    // M1
            {0, 0, 0, 30, 80, 300},    // M2
            {0, 0, 0, 25, 50, 200},    // M3
            {0, 0, 0, 20, 40, 150},    // M4
            {0, 0, 0, 15, 40, 120},    // M5
            {0, 0, 0, 5, 30, 80},    // F6
            {0, 0, 0, 5, 20, 80},    // F7
            {0, 0, 0, 5, 20, 60},    // F8
            {0, 0, 0, 4, 20, 50},    // F9
            {0, 0, 0, 4, 20, 50},    // F10

            {0, 0, 0, 0, 0, 0, 0},   //BN

            {0, 0, 0, 0, 0, 0, 0},   //RANDS
            {0, 0, 0, 0, 0, 0, 0},   //RANDS
            {0, 0, 0, 0, 0, 0, 0},   //RANDS
            {0, 0, 0, 0, 0, 0, 0},   //RANDS
            {0, 0, 0, 0, 0, 0, 0},   //RANDS


    };


    //reels
    public static final int[][] BaseGameGrids = {
            { F8, M1, M1, F6, F7, M5, F10, F10, M5, F6, M2, M2, F7, F9, M3, M3, F6, M1, F8, F6, M2, F9, F7, M5,
                    F6, F6, M5, F7, F6, M5, F9, F8, M5, F9, F9, M4, M4, F6, M5, M5, F6, F6, M2, F9, F9, M5, F7, BN, F6, M5,
                    F7, F6, M5, F9, M1, F8, M5, F9, BN, F10, M5, F6, M4, F10, F10, M5, F9, M2, F6, M1, F9, F10, M5, F9, M2,
                    F6, M3, F9, M2, F6, F9, M5, F6, F6, M5, F7, F6, M5, F8, F6, M1, F10, M3, F6, M5, F9, M1, F6, F9},
            {WR,M5, F10, F10, M3, F8, M4, F6, F6, M2, F10, F10, F6, M5, F9, BN, F7, M3, F8, M5, M3, M4, F9, BN, F7,
                    F10, M3, F8, M1, F7, F7, BN, F8, M5, F10, M1, F6, M3, F8, F8, M4, F10, M2, F9, F8, M2, F7, F9, M4, F8,
                    M3, F7, M2, F10, M4, F7, BN, F8, M3, F6, M5, F8, M2, F6, M4, F8, BN, F6, M4, F10, M3, F6, F6, M4, F7,
                    M3, F6, M5, F10, M2, F8, M5, F10, F10, M2, F8, M5, F7, BN, F6, M4, F9, M2, F6, M3, F10, F7, M4, F10, F9},
            {WR, F10, M1, M1, F7, M2, F8, F9, M4, F8, F10, M4, F8, F7, M1, F9, F9, M4, F10, F8, M3, F7, F9, M5, M3,
                    F9, F9, M3, F6, F10, M3, F7, F8, M3, F7, F8, M1, M4, F9, F7, M4, F9, M5, F6, F10, M4, M2, F8, F7, M1,
                    F10, F6, M4, M2, F7, F8, M5, M3, F9, F8, M3, F10, F10, M3, M3, F7, BN, F8, F8, M3, F10, F10, M4, F8,
                    M1, F7, M4, M1, F9, M3, F10, F8, M3, F7, F9, M4, M4, F9, F9, M3, M3, F10, F8, M3, M5, F7, F7, M4, M1, F9},
            {WR, F9, M4, M4, F8, F8, M5, F7, F7, M5, F6, F6, M5, F9, F9, M3, F7, M2, M2, F6, M3, F8, F8, M5, F9, F9,
                    M4, F7, M1, M1, F9, F9, M5, F6, F9, M4, F7, F9, F9, M3, F6, F6, M5, F9, F9, M5, F7, F7, M4, F8, F8, M2,
                    F9, F9, M2, F8, F8, M2, M2, F9, F9, F7, F7, M5, M5, F6,BN, F6, M4, M4, F7, M3, F8, F8, M5, F9, F9, M4, M4,
                    F8, F8, M1, F10, F10, M1, F8, F8, M4, F9, F9, M3, F7, M1, F8, M3, F10, F10, M4, M4, F7, F7, F9, M4, M4,
                    F8, F8, M5, F7, F7, M5, F6, F6, M5, F9, F9, M3, F7, M2, M2, F6, M3, F8, F8, M5, F9, F9, M4, F7, M1, M1,
                    F9, F9, M5, F6, F9, M4, F7, F9, F9, M3, F6, F6, M5, F9, F9, BN, F7, F7, M4, F8, F8, M2, M2, F9, F9, F8,
                    F8, M2, M2, F9, F9, F7, F7, M5, M5, F6, F6, M4, M4, F7, F7, M3, F8, F8, M5, F9, F9, M4, M4, F8, F8, M1,
                    M1, F8, F8, F10, F10, M4, F9, F9, M3, F7, M1, M3, F10, F10, M4, M4, F7, F7},
            {WR, F10, F10, M1, M1, F7, M4, F8, F8, M3, F10, F10, M4, F10, F10, BN, F10, F10, M3, BN, F10, F10, M5,
                    M5, F10, F10, M3, M5, F8, F8, BN, F10, F10, M2, M2, F10, F10, M5, M1, F7, F7, M4, M4, M3, M3, F6, F7,
                    M2, F8, BN, F7, F10, M2, F7, M3, F9, F6, M5, F8, M2, M2, F10, F10, M5, F7, BN, F9, M1, F10, BN, M4,
                    F10, F10, M3, F8, F8, M1, M1, F7, F7, M4, F10, F10, M1, M1, BN, M4, M5, M5, F6, M3, F10, F6, M5, F10,
                    F10, M1, F7, M3, F10, F10, M1, M1, F10, F10, M4, F8, F8, M3, F10, F10, M4, F10, F10, BN, F10, F10,
                    M3, BN, F10, F10, M5, M5, F10, F10, M3, M5, F8, F6, BN, F10, F10, M2, M2, F10, F10, M5, M1, F7, F7,
                    M4, M4, M3, M3, F6, F7, M2, F6, BN, F9, F10, M2, F7, F8, M3, F9, M5, F8, M2, M2, F10, F10, M5, F7, BN,
                    F9, M1, F10, M4, F10, F10, M3, F8, F8, M1, M1, F7, F7, M4, F10, F10, M1, M1, F9, BN, M4, M5, M5, F6,
                    M3, F10, M5, F10, F10, M1, F6, M3, F10, F10}};

    public static final int[][] FreeGameGrids = {
            {M5, F10, F10, M4, F6, F6, M2, F10, F10, M3, F6, F8, M5, F9, F9, M3, F7, F8, M3, M5, F7, F8, M4, F9, M4,
                    F8, F10, M3, M1, F7, F7, M4, F7, F6, M5, F10, M3, M3, F9, F8, M4, F8, F10, M2, F8, F9, M2, F7, F6, M4,
                    M3, F7, F8, M2, F8, F6, M4, F7, M3, F8, M3, F6, F6, M2, F6, F6, M4, F10, F6, M4, M3, F6, F8, M4, M3, F6,
                    F6, M5, F10, F8, M5, M2, F10, F10, M2, M5, F8, F7, M4, F7, F6, M2, F6, F6, F10, F10, M3, M4, F10, F9},
            {WR, F8, M5, M3, F6, F7, M5, F10, F10, M5, F6, F7, M2, M2, M2, M3, F9, F6, M1, F8, F6, M2, F6, F9, M5,
                    F6, F6, F7, F6, M5, F9, F8, M5, F9, F9, M4, M5, F9, F6, F6, M2, F9, F6, M5, F9, F7, M5,  F6, M5,
                    F6, F7, M5, F9, F10, M5, M1, F9, M5, F8, F6, M5, M4, F10, F10, M5, F9, F6, M2, M1, F9, F6, M5, F9, M2,
                    F6, F9, M3, M2, F6, F9, M5, F6, F6, M5,F7, F6, M5, F8, M1, F6, F9, M5, M4, F9, F9, M1, F6, F9},
            {WR, F10, M1, F8, F7, M2, F8, F9, M4, F8, F10, M4, F8, M1, F9, F9, M4, F10, F8, M3, F7, F9, M1, M3,
                    F9, F9, M3, F6, F10, M3, F7, F8, M3, F7, F8, M1, F9, F9, F7, M4, F9, M5, WR, F10, M4, M2, F8, F7, M1,
                    F10, F6, M4, M2, F7, F8, M5, M3, F9,F8, M3, F10, F10, M1, M3, F8, F7, M3, F8, F9, M5, F10, F10, M4,
                    F8, F7, M4, M1, F9, M3, F10, F8, M3, F7, F9, M1, M4, F9, F9, M3, M3, F10, F8, M3, M5, F7, F7, M4, M1, F9},
            {WR, F9, F6, M4, M4, F6, F8, M5, F7, F7, M5, F6,F6, M5, F9, F9, M3, F7, M2, M2, M3, F8, F8, M5, F9, F9,
                    M4, F7, M1, M1, F10, F10, M5, F6, M4, M4, F7, F9, F9, M3, F6, F8, M5, F10, F10, M4, F7, F7, M4, F8, F8,
                    M2, M2, F10, F10, F8, F8,  M5, M2, F9, F9, F7, F7, F7, M5, F6, F6, M4, F8, F7, M3, F8, F8, M5, F10,
                    F10, M4, F7, F8, M1, M1, F8, F8, M1, F10, F10, M4, F10, F10, M3, F7, M1, M4, F10, F10, F10, M4, F7, F7},
            {WR, F10, F10, M1, M1, M4, F8, F8, M3, F10, F10, M4, M4, F10, F10, M4, F10, F10, M3, F9, F9, M5,
                    M5, F7, F10, M3, M5, F8, F6, F6, F10, M2, M2, M2, F10, F10, M5, M1, F7, F7, M4, M4, M3, M3, F6, F7, M2,
                    F6, F9, F10, M2, F7, F8, M3, F9, F6, M5, F8, M2, M2, F10, F10, M5, F7, F7, F7, M1, F9, F9, M4, F10,
                    F10, M3, M1, F10, F7, F7, WR,M4, F10, F10, M1, M1, M3, M4, M5, M1, F6, F7, M3, M5, F10, F10, M1, F7, F10,
                    F10, M3, F6, F8}};

    // WeightedRandomArray


    // * Bonus Reel Content
    public static final WeightedRandomArray horizontalReelContentReel1 = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {symbolSplitSymbol, 100},});
    public static final WeightedRandomArray horizontalReelContentReel2 = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 80}, {symbolSplitSymbol, 100}, {symbolSplitWR, 100}});
    public static final WeightedRandomArray horizontalReelContentReel3 = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 80}, {symbolSplitSymbol, 100}, {symbolSplitWR, 100}});
    public static final WeightedRandomArray horizontalReelContentReel4 = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 80}, {symbolSplitSymbol, 100}, {symbolSplitWR, 100}});
    public static final WeightedRandomArray horizontalReelContentReel5 = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 80}, {symbolSplitSymbol, 100}, {symbolSplitWR, 100}});


    public static final WeightedRandomArray horizontalReelContentReel1FS = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {symbolSplitSymbol, 100},});
    public static final WeightedRandomArray horizontalReelContentReel2FS = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 30}, {symbolSplitSymbol, 100}, {symbolSplitWR, 50}});
    public static final WeightedRandomArray horizontalReelContentReel3FS = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 30}, {symbolSplitSymbol, 100}, {symbolSplitWR, 50}});
    public static final WeightedRandomArray horizontalReelContentReel4FS = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 30}, {symbolSplitSymbol, 100}, {symbolSplitWR, 50}});
    public static final WeightedRandomArray horizontalReelContentReel5FS = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30},
            {multiplierWR, 30}, {symbolSplitSymbol, 100}, {symbolSplitWR, 50}});


    public static final WeightedRandomArray symbolsplitTurnTo = new WeightedRandomArray(new int[][]{
            {M1, 30}, {M2, 30},{M3, 30},{M4, 30},{M5, 30}});

    public static final WeightedRandomArray multiplierWRmultipliers = new WeightedRandomArray(new int[][]{
            {2, 80}, {4, 100}});

    public static final WeightedRandomArray symbolSplitConfig = new WeightedRandomArray(new int[][]{
            {2, 500}, {3, 100}, {4, 50}, });

    // * Basic Configs
    public static final int BASE_BET = 20;
    public static final int W = 5;
    public static final int H = 3;

    public static final int N_FS_AWARDED = 8;
    public static final int N_RETRIGGER_AWARD = 0;
    public static final int[] SCATTER_PAYS = new int[]{0, 0, 0, 0, 0, 0}; //times bet // no scatter pay in this game

    public static final String VERSION = "skulls_up_2_nWays";

}
