package com.quickspin.prototyping.game.prototype;

import casinoworld.games.Slot;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.quickspin.prototyping.SlotGame;
import com.quickspin.prototyping.configuration.SlotGameSettings;
import com.quickspin.prototyping.configuration.SlotSymbolProperties;
import com.quickspin.prototyping.grid.layout.GridLayout;
import com.quickspin.prototyping.grid.layout.SpinReelLayout;
import com.quickspin.prototyping.grid.symbol.SymbolDefinition;
import com.quickspin.prototyping.outcome.FreespinsOutcome;
import com.quickspin.prototyping.game.GameMath;
import com.quickspin.prototyping.outcome.IForcedOutcome;
import com.quickspin.prototyping.server.IPrototypeSlot;

public class Main {
    public static void main(String[] args) {
        final GamePrototype prototype = SlotGame.init(GamePrototype.class);
        GameMath.runPrototype = true;

        prototype.getConfig()
                .withTitle(GameMath.GAME_NAME)
                .withVersion(GameMath.VERSION)
                .withCreditsPerBet(GameMath.CREDITSPERBET)
                .withGameWidth((102 * 5) + 120)
                .withGameHeight((94 * 3) + 160)
                .withGameRules("https://quickspin.atlassian.net/wiki/spaces/MATH/pages/1835040840/Book+of+Hero")
                .withSymbolWidth(102)
                .withSymbolHeight(94)
                .withGridLayout(new GridLayout(3, 3, 3, 3, 3)
                        .setReels(
                                new SpinReelLayout().setSymbolDefinition(SymbolDefinition.TextImageSymbol),
                                new SpinReelLayout().setSymbolDefinition(SymbolDefinition.TextImageSymbol),
                                new SpinReelLayout().setSymbolDefinition(SymbolDefinition.TextImageSymbol),
                                new SpinReelLayout().setSymbolDefinition(SymbolDefinition.TextImageSymbol),
                                new SpinReelLayout().setSymbolDefinition(SymbolDefinition.TextImageSymbol)
                        ))
                .withDefaultReelPositions(0)
                .shouldMaintainAspectRatio(true)
                .withSetting(SlotGameSettings.Symbol_TextFontPath, "fonts/BebasNeue-Regular.ttf")

                //symbols
                .withSymbol(GameMath.M1, new SlotSymbolProperties("symbols/M1.png"))
                .withSymbol(GameMath.M2, new SlotSymbolProperties("symbols/M2.png"))
                .withSymbol(GameMath.M3, new SlotSymbolProperties("symbols/M3.png"))
                .withSymbol(GameMath.M4, new SlotSymbolProperties("symbols/M4.png"))
                .withSymbol(GameMath.M5, new SlotSymbolProperties("symbols/M5.png"))

                .withSymbol(GameMath.F6, new SlotSymbolProperties("symbols/F6.png"))
                .withSymbol(GameMath.F7, new SlotSymbolProperties("symbols/F7.png"))
                .withSymbol(GameMath.F8, new SlotSymbolProperties("symbols/F8.png"))
                .withSymbol(GameMath.F9, new SlotSymbolProperties("symbols/F9.png"))
//                .withSymbol(GameMath.BN, new SlotSymbolProperties("symbols/BN.png"))
                .withSymbol(GameMath.WR, new SlotSymbolProperties("symbols/WR.png"))

                .withForcedOutcome("Freespins", new FreespinsOutcome())


                // * customized forced outcomes FO
                .withForcedOutcome("FO test: 2WR ", new IForcedOutcome() {
                    @Override
                    public boolean checkOutcome(Slot slot) {
                        GameMath.MathSpin baseSpin = (GameMath.MathSpin) ((IPrototypeSlot) slot).getBaseSpin();
                        return baseSpin.nBN == 2;
                    }
                })
                .withForcedOutcome("FO test: 3WR ", new IForcedOutcome() {
                    @Override
                    public boolean checkOutcome(Slot slot) {
                        GameMath.MathSpin baseSpin = (GameMath.MathSpin) ((IPrototypeSlot) slot).getBaseSpin();
                        return baseSpin.nBN == 3;
                    }
                })
                .withForcedOutcome("FO test: 4WR ", new IForcedOutcome() {
                    @Override
                    public boolean checkOutcome(Slot slot) {
                        GameMath.MathSpin baseSpin = (GameMath.MathSpin) ((IPrototypeSlot) slot).getBaseSpin();
                        return baseSpin.nBN == 4;
                    }
                })
                .withForcedOutcome("FO test: 5WR ", new IForcedOutcome() {
                    @Override
                    public boolean checkOutcome(Slot slot) {
                        GameMath.MathSpin baseSpin = (GameMath.MathSpin) ((IPrototypeSlot) slot).getBaseSpin();
                        return baseSpin.nBN == 5;
                    }
                })
                .withForcedOutcome("FO test: Retrigger ", new IForcedOutcome() {
                    @Override
                    public boolean checkOutcome(Slot slot) {
                        GameMath.MathSpin baseSpin = (GameMath.MathSpin) ((IPrototypeSlot) slot).getBaseSpin();
                        return baseSpin.fsRetriggerCount > 0;
                    }
                })
        ;


        new Lwjgl3Application(prototype, prototype.getConfig().getAppConfig());
    }
}
