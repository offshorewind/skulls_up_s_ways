package com.quickspin.prototyping.game;

import casinoworld.achievements.UnibetCityRewards;
import casinoworld.games.GameRound;
import casinoworld.games.Slot;
import casinoworld.games.helpers.*;
import casinoworld.player.Player;
import com.quickspin.prototyping.feature.result.IFreespinRetrigger;
import com.quickspin.prototyping.feature.result.IFreespins;
import com.quickspin.prototyping.GameDefinition;
import com.quickspin.prototyping.server.PrototypeRNG;
import com.quickspin.prototyping.server.PrototypeSlot;
import com.quickspin.prototyping.server.PrototypeSlotSpin;
import com.quickspin.prototyping.server.ResultType;
import com.quickspin.prototyping.game.data.Model;
import org.json.JSONObject;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import rng.RNG;

import java.util.*;

@Component(GameDefinition.Static.Simulation)
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class GameMath extends PrototypeSlot {

    public static boolean runPrototype = true;

    static {
        GAME_NAME = "Skulls up 2 nways";
        VERSION = "prototype.0.1";
        CREDITSPERBET = Model.BASE_BET;
        int[] bet_mul = new int[]{1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 30, 40, 60, 80, 160, 200, 400};
        BETSIZES = new int[bet_mul.length];
        for (int i = 0; i < BETSIZES.length; i++) {
            BETSIZES[i] = bet_mul[i] * CREDITSPERBET;
        }
    }


    /* Game configuration: */
    public static final int WR = Model.WR;

    public static final int M1 = Model.M1;
    public static final int M2 = Model.M2;
    public static final int M3 = Model.M3;
    public static final int M4 = Model.M4;
    public static final int M5 = Model.M5;

    public static final int F6 = Model.F6;
    public static final int F7 = Model.F7;
    public static final int F8 = Model.F8;
    public static final int F9 = Model.F9;
    public static final int F10 = Model.F10;

    public static final int BN = Model.BN;

    public static final int multiplierWR = Model.multiplierWR;
    public static final int symbolSplitSymbol = Model.symbolSplitSymbol;
    public static final int symbolSplitWR = Model.symbolSplitWR;

    // locked position for bonus reel presentation
    public static final int Block = 99;

 /*   //RAND per reel
    public static final int RAND1 = Model.R1;
    public static final int RAND2 = Model.R2;
    public static final int RAND3 = Model.R3;
    public static final int RAND4 = Model.R4;
    public static final int RAND5 = Model.R5;

    public static final int[] RANDS_REELS = new int[]{RAND1, RAND2, RAND3, RAND4, RAND5};*/

    // must match above symbols
    public static final String[] symNames = new String[]{"",
            "WR", //index 1
            "M1", "M2", "M3", "M4", "M5",
            "F6", "F7", "F8", "F9", "F10",
            "BN",
            "multiplierWR", "symbolSplitMedium", "symbolSplitLow",
    };

    public static final int[] PAYING_SYMS = new int[]{M1, M2, M3, M4, M5, F6, F7, F8, F9, F10};

    public static final boolean[] SYM_POOL_BOOLEANS = new boolean[]{false, false, //WR
            true, true, true, true, true, //M
            true, true, true, true, true,  //F
            false, //BN
            false, false, false
    };

    public static final boolean[] isWild = new boolean[symNames.length];
    public static final boolean[] isMedium = new boolean[symNames.length];
    public static final boolean[] isLow = new boolean[symNames.length];

    static {
        isWild[WR] = true;
        isMedium[M1] = true;
        isMedium[M2] = true;
        isMedium[M3] = true;
        isMedium[M4] = true;
        isMedium[M5] = true;
        isLow[F6] = true;
        isLow[F7] = true;
        isLow[F8] = true;
        isLow[F9] = true;
        isLow[F10] = true;
    }

    // PAY_OUTS
    public static final int[][] PAY_OUTS = Model.PAY_OUT;


    //    PARAMETERS
    public static final int W = Model.W, H = Model.H;
    public static final int N_FS_AWARDED = Model.N_FS_AWARDED;
    static final int[] SCATTER_PAYS = Model.SCATTER_PAYS;

    private static final int N_RETRIGGER_AWARD = Model.N_RETRIGGER_AWARD;

    // WRA
    public static final WeightedRandomArray horizontalReelContentReel1 = Model.horizontalReelContentReel1;
    public static final WeightedRandomArray horizontalReelContentReel2 = Model.horizontalReelContentReel2;
    public static final WeightedRandomArray horizontalReelContentReel3 = Model.horizontalReelContentReel3;
    public static final WeightedRandomArray horizontalReelContentReel4 = Model.horizontalReelContentReel4;
    public static final WeightedRandomArray horizontalReelContentReel5 = Model.horizontalReelContentReel5;

    public static final WeightedRandomArray horizontalReelContentReel1FS = Model.horizontalReelContentReel1FS;
    public static final WeightedRandomArray horizontalReelContentReel2FS = Model.horizontalReelContentReel2FS;
    public static final WeightedRandomArray horizontalReelContentReel3FS = Model.horizontalReelContentReel3FS;
    public static final WeightedRandomArray horizontalReelContentReel4FS = Model.horizontalReelContentReel4FS;
    public static final WeightedRandomArray horizontalReelContentReel5FS = Model.horizontalReelContentReel5FS;

    public static final WeightedRandomArray[] horizontalReelsContentReels = {horizontalReelContentReel1,
            horizontalReelContentReel2, horizontalReelContentReel3, horizontalReelContentReel4,
            horizontalReelContentReel5};

    public static final WeightedRandomArray[] horizontalReelsContentReelsFreeSpin = {horizontalReelContentReel1FS,
            horizontalReelContentReel2FS, horizontalReelContentReel3FS, horizontalReelContentReel4FS,
            horizontalReelContentReel5FS};

    public static final WeightedRandomArray multiplierWRmultipliers = Model.multiplierWRmultipliers;
    public static final WeightedRandomArray symbolSplitConfig = Model.symbolSplitConfig;


    // Grids
    public static final List<int[][]> reelContainer = new ArrayList<>();

    static {
        reelContainer.add(0, Model.BaseGameGrids);
        reelContainer.add(1, Model.FreeGameGrids);
    }

    private static final int[][] SymbolsWeightTemplate = {
            {0, 0},
            {WR, 0},
            {M1, 0},
            {M2, 0},
            {M3, 0},
            {M4, 0},
            {M5, 0},
            {F6, 0},
            {F7, 0},
            {F8, 0},
            {F9, 0},
    };


    public static final int[][] symbolSplitMatrixDefault = {{0, 1, 1, 1}, {0, 1, 1, 1}, {0, 1, 1, 1}, {0, 1, 1, 1}, {0, 1, 1, 1},};

    public GameMath(RNG rng, Player player) {
        super(rng, player);
    }

    public GameMath() {
        this(new PrototypeRNG(), null);
    }

    @Override
    protected int[] getPossibleBetSizes() {
        return BETSIZES;
    }


    protected int[] spinReels(int[][] reels) {
        return Slot.spinReels(reels, rng);
    }


    public class MathSpin extends PrototypeSlotSpin implements IFreespins, IFreespinRetrigger {

        // * bonus reel content
        public int[] bonusReelContent;
        public boolean[] bonusReelOpenStatus;

        public boolean hasFeature;
        public int featWin;

        public int[][] symbolSplitMatrix;
        public int[][] finalSymsForPresentation;
        public int[][] finalSymsForEvaluation;
        public int finalWin;

        public int nBN;
        int[] hasBNreels = {0, 0, 0, 0, 0};
        int[] hasWRreels = {0, 0, 0, 0, 0};

        public boolean hasFs;
        public boolean isFs;
        public int expensionSymInFs;
        public int fsRetriggerCount = 0;

        public List<SlotWin> featureWins;

        protected MathSpin(ResultType type, int[] stops, int[][] reels, boolean[] bonusReelOpenStatusFS) { //base spin

            // create normal spin
            super(stops, reels, H);

            this.iniSyms = super.getOriginalSymbolMatrix();
            this.isFs = type == ResultType.Freespin;

            //create bonus reels and set symSplitMatrix to default
            generatebonusReelContent();
            if (!isFs)
                this.bonusReelOpenStatus = new boolean[]{false, false, false, false, false};
            else
                this.bonusReelOpenStatus = bonusReelOpenStatusFS.clone();

            this.symbolSplitMatrix = deepCloneSyms(symbolSplitMatrixDefault);

            //todo do some symbol split here

            //check if do feature/freespins
            this.nBN = SlotUtils.countScatterSymbols(BN, this.iniSyms);
            updateHasBNreels(); // update hasBNreels

//            if(isFs){
//                //todo do random WR feature
//                doRandomWR(bonusReelOpenStatusFS);
//            }

            if (this.nBN >= 3)
                hasFs = true;

            // * SECTION 2. open up bonus reel locations
            if(!isFs) {
                if (this.nBN > 0) {
                    hasFeature = true;
                    updateBonusReelsOpenStatus();
                    openLockedLocations();
                    updatefinalSymsForEvaluationAfterFeature();

                    //todo add featurewins correctly
//                finalWin = getEvalScreen(finalSymsForEvaluation);
//                featWin = finalWin;
                } else {
                    finalSymsForEvaluation = deepCloneSyms(iniSyms);
                }
            }else{
                hasFeature = true;
                updateBonusReelsOpenStatus();
                openLockedLocations();
                updatefinalSymsForEvaluationAfterFeature();
            }


            // * DEBUGGING LINE
//            finalSymsForEvaluation = deepCloneSyms(iniSyms);


            //TODO evaluate matrix finalSymsForEvaluation instead.
            this.win = getTotalCashWins();
        }

        private void doRandomWR(boolean[] bonusReelOpenStatusFS) {
            for (int i = 0; i < bonusReelOpenStatusFS.length; i++) {
                if(bonusReelOpenStatusFS[i]= true){
                    int x = rng.getNextInt(1,4);
                    int y = rng.getNextInt(0,2);
                    iniSyms[x][y] = WR;
                }
            }
        }


        private void updatefinalSymsForEvaluationAfterFeature() {

//            finalSymsForPresentation
//            symbolSplitMatrix

            finalSymsForEvaluation = new int[W][];
            for (int i = 0; i < W; i++) {

                int height = 0;
                for (int j = 0; j < H+1; j++) {
                    height += symbolSplitMatrix[i][j];
                }

                finalSymsForEvaluation[i] = new int[height];
                //todo check
                int counter = 0;
                for (int k = 0; k < H + 1; k++) { // k is 0,1,2,3
                    for (int l = 0; l < symbolSplitMatrix[i][k]; l++) {
                        finalSymsForEvaluation[i][counter] = finalSymsForPresentation[i][k];
                        counter++;
                    }
                }

            }
            //finish updating finalSymsForEvaluation
        }


        private int getEvalScreen(int[][] syms) {
            int result = 0;

/*            for (int i = 0; i < W; i++) {
                nReels += expansionReels[i];
            }
            result = PAY_OUTS[symPick][nReels] * CREDITSPERBET;*/

            return result;
        }

        private void generatebonusReelContent() {
            this.bonusReelContent = new int[W];
            if(!isFs) {
                for (int i = 0; i < W; i++) {
                    this.bonusReelContent[i] = horizontalReelsContentReels[i].rollSingleValue(rng);
                }
            }
            else{
                for (int i = 0; i < W; i++) {
                    this.bonusReelContent[i] = horizontalReelsContentReelsFreeSpin[i].rollSingleValue(rng);
                }
            }

        }


        private void openLockedLocations() {

            //set finalSymsForPresentation to default
            finalSymsForPresentation = new int[W][H + 1];
            for (int i = 0; i < W; i++) { //i is 0,1,2,3,4
                finalSymsForPresentation[i][0] = Block;
                for (int j = 1; j < H + 1; j++) { //j is 1,2,3
                    finalSymsForPresentation[i][j] = iniSyms[i][j - 1]; //j-1 is 0,1,2
                }
            }

            //todo do the bonus effect here
            for (int i = 0; i < W; i++) {
                if (bonusReelOpenStatus[i]) {

                    finalSymsForPresentation[i][0] = bonusReelContent[i];

                    //todo update finalSymsForPresentation, symbolSplitMatrix, with multiplier and split symbols

                    // * a multiplier WR
                    if (bonusReelContent[i] == multiplierWR) {
                        finalSymsForPresentation[i][0] = WR;
                        symbolSplitMatrix[i][0] = multiplierWRmultipliers.rollSingleValue(rng);

                    // * a symbol that split normal symbols
                    } else if (bonusReelContent[i] == symbolSplitSymbol) {
                        //put a medium symbol there
                        finalSymsForPresentation[i][0] = Model.symbolsplitTurnTo.rollSingleValue(rng);
                        symbolSplitMatrix[i][0] = 1;
                        for (int j = 1; j < H + 1; j++) {
                            symbolSplitMatrix[i][j] = symbolSplitConfig.rollSingleValue(rng);
                        }

                    // * a WR that splits normal symbols
                    } else if (bonusReelContent[i] == symbolSplitWR) {
                        finalSymsForPresentation[i][0] = WR;
                        symbolSplitMatrix[i][0] = 1;
                        for (int j = 1; j < H + 1; j++) {
                            symbolSplitMatrix[i][j] = symbolSplitConfig.rollSingleValue(rng);
                        }

                    // a normal Medium symbol
                    } else if (isMedium[bonusReelContent[i]]) {
                        symbolSplitMatrix[i][0] = 1;
                        //do nothing
//                        symbolSplitMatrix[i][0] = 4;
                    }
                }
            }

        }


        private void updateHasBNreels() {
            for (int i = 0; i < W; i++) {
                for (int j = 0; j < H; j++) {
                    if (this.iniSyms[i][j] == BN) {
                        /*if (nBN == 2)
                            iniSyms[i][j] = WR; //todo check WR on reel 1 cost*/
                        hasBNreels[i]++;
                    }
                    if (this.iniSyms[i][j] == WR) {
                        hasWRreels[i]++;
                    }
                }
            }
        }


        private void updateBonusReelsOpenStatus() {

            if (!isFs) {
                for (int i = 0; i < W; i++)
                    if (this.hasBNreels[i] > 0)
                        bonusReelOpenStatus[i] = true;
            } else {
                for (int i = 0; i < W; i++)
                    if (this.hasWRreels[i] > 0)
                        bonusReelOpenStatus[i] = true;
            }

        }


        private void handleScatterPays() {
            long scatterWin = SCATTER_PAYS[nBN] * totalBet;

            if (scatterWin > 0) {
                List<Coordinates> BNCoords = new ArrayList<>();
                for (int x = 0; x < W; x++) {
                    for (int y = 0; y < H; y++) {
                        if (iniSyms[x][y] == WR) {
                            BNCoords.add(new Coordinates(x, y));
                        }
                    }
                }

                this.win += scatterWin;
                getWins().add(new SlotWin(SlotWin.SlotWinType.scatter, scatterWin, nBN, WR, 0, 1, 1, BNCoords, false));
            }
        }


        public int[][] getFinalSymbolMatrix() {
            return this.finalSymsForEvaluation;

            //DEBUGGING
//            return new int[][]{{M1,M2,M3},{M1,M5,F6},{M1,M1,M1,F9},{M4,M5,F6},{F7,F8,F9}};
        }

        @Override
        public List<SlotWin> getWins() {
            if (wins == null) {
                //todo set up multiplier
                int endMultiplier = 1;
                wins = WaysSlotUtils.findWaysWins(win -> win * endMultiplier * totalBet / CREDITSPERBET, getFinalSymbolMatrix(), PAY_OUTS, PAYING_SYMS, isWild);

            }


            //TODO CHECK THE WINNINGS

            return wins;
        }


        @Override
        public int getTotalSpinCount() {
            return N_FS_AWARDED;
        }


        public boolean triggersFreespins() {
            return hasFs;
        }

        @Override
        public int getAwardedSpins() {
            if (this.nBN >= 3)
                return N_RETRIGGER_AWARD;
            else
                return 0;
        }
    }


    private void replaceRANDS(int[] from, List<Integer> to, int[][] iniSyms) {
        for (int i = 0; i < from.length; i++) {
            replaceSymbols(from[i], to.get(i), iniSyms);
        }
    }


    public static int[][] deepCloneSyms(int[][] syms) {
        int[][] out = new int[syms.length][];
        for (int i = 0; i < syms.length; ++i) {
            out[i] = syms[i].clone();
        }
        return out;
    }


    public void playGame() {
        baseWin = 0;
        featureWin = 0;
        fsWin = 0;

        // * SECTION 1
        int[][] baseReels = reelContainer.get(0);
        MathSpin base = new MathSpin(ResultType.Base, spinReels(baseReels, rng), baseReels, null);
        baseSpin = base;
        baseWin = base.win;
        featureWin += base.featWin;

        base.handleScatterPays();

        // * SECTION 3 - FREESPINS
        if (base.triggersFreespins()) {
            handleFreeSpins();
        }

        totalWin = baseWin + featureWin + fsWin;
    }


    private void handleFreeSpins() {

        int nFs = N_FS_AWARDED;
        int[][] fsReels = reelContainer.get(1);

        boolean[] bonusReelOpenStatusFS = ((MathSpin) baseSpin).bonusReelOpenStatus.clone();

        for (int n = 0; n < nFs; n++) {

            MathSpin fs = new MathSpin(ResultType.Freespin, spinReels(fsReels, rng), fsReels, bonusReelOpenStatusFS);

            fsWin += fs.win + fs.featWin;

/*          //No retrigger in the game yet
            if (fs.nBN >= 3) {
                nFs += N_RETRIGGER_AWARD; //adding re-trigger
                ((MathSpin) baseSpin).fsRetriggerCount++;
            }*/

            freespins.add(fs);
        }
    }

    @Override
    public JSONObject getGameDescription() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    protected GameRound getInstance(RNG rng, String s, GameRound gameRound, Player player, boolean forcedOutcomes) {
        throw new RuntimeException("Not Implemented");
    }

    @Override
    public void playGame(JSONObject arguments, UnibetCityRewards rewardProgram, Player player) {
        throw new RuntimeException("Not Implemented");
    }

    @Override
    public double getEv() {
        return 0;
    }

    @Override
    public double getStd() {
        return 0;
    }

    @Override
    public int getGameId() {
        return 0;
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }


}
