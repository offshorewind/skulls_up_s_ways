package com.quickspin.prototyping.game.prototype;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.quickspin.prototyping.configuration.SlotGameSettings;
import com.quickspin.prototyping.ui.CustomUI;
import com.quickspin.prototyping.utils.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AdditionalUI extends CustomUI {
    private Label _rainbow;

    @PostConstruct
    @Override
    protected void postConstruct() {
        super.postConstruct();
        String fontPath = StringUtils.getSettingValue(this._config.getSettings(), SlotGameSettings.Splash_TextFontPath);
        if (fontPath != "") {
            Label.LabelStyle s = new Label.LabelStyle((BitmapFont)this._assets.getManager().get(fontPath), Color.WHITE);
            this._rainbow = new Label("0", s);
            this._rainbow.setPosition(25, -20);
            this.addActor(this._rainbow);
        }
    }

    public void setCoins(int num) {
        _rainbow.setScale(1);
        _rainbow.setText("" + num);
    }
}
