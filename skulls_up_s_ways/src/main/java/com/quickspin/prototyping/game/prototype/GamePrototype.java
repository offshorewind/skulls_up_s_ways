package com.quickspin.prototyping.game.prototype;

import casinoworld.games.helpers.SlotWin;
import com.quickspin.prototyping.GameDefinition;
import com.quickspin.prototyping.SlotGame;
import com.quickspin.prototyping.game.GameMath;
import com.quickspin.prototyping.grid.GridDefinition;
import com.quickspin.prototyping.sequence.FreespinSequenceHelper;
import com.quickspin.prototyping.sequence.GameSequenceHelper;
import com.quickspin.prototyping.sequence.PaylineSequenceHelper;
import com.quickspin.prototyping.sequence.UISequenceHelper;
import com.quickspin.prototyping.server.ResultType;
import com.quickspin.prototyping.ui.BigWinUI;
import com.quickspin.prototyping.ui.FreespinsCustomUI;
import com.quickspin.prototyping.utils.ActorAlign;
import com.quickspin.prototyping.utils.sequence.Sequence;
import com.quickspin.prototyping.utils.sequence.items.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(GameDefinition.Static.GamePrototype)
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class GamePrototype extends SlotGame {
    @Autowired
    private FreespinSequenceHelper _fsHelper;
    @Autowired
    private PaylineSequenceHelper _payHelper;
    @Autowired
    private UISequenceHelper _uiHelper;
    @Autowired
    private GameSequenceHelper _gameHelper;

    @Override
    protected Sequence startUp() {
        return new Sequence(
                new NestedSequenceByFunctionSequenceItem(_presetHelper.defaultLoadAssets(new ISequenceItemSequence() {
                    @Override
                    public Sequence execute(Object[] objects) throws Exception {
                        return new Sequence(
                                new FunctionSequenceItem(new ISequenceItemFunction() {
                                    @Override
                                    public void execute(Object[] objects) throws Exception {

                                    }
                                })
                        );
                    }
                })),
                new FunctionSequenceItem(_gridHelper.addDefaultGrid(0, GridDefinition.SpinningReelGrid)),
                new FunctionSequenceItem(_gridHelper.setRandomGrid(0)),
                new FunctionSequenceItem(_uiHelper.setCustomUI(FreespinsCustomUI.class)),
                new FunctionSequenceItem(_uiHelper.setCustomUI(1, BigWinUI.class)),
                new FunctionSequenceItem(_uiHelper.setVisibleCustomUI(0, false)),
                new FunctionSequenceItem(_uiHelper.setVisibleCustomUI(1, false)),
                new FunctionSequenceItem(_gameHelper.setBalance(10000)),
                new FunctionSequenceItem(_gridHelper.repositionGrid(0, ActorAlign.Horizontal.Center, ActorAlign.Vertical.Center, 0, 30))
        );
    }

    @Override
    protected Sequence spin() {
        return new Sequence(
                new FunctionSequenceItem(_uiHelper.setVisibleCustomUI(1, true)),
                new NestedSequenceByFunctionSequenceItem(_presetHelper.defaultSpinStart()),
                new FunctionSequenceItem(_resultHelper.setResult(ResultType.Base)),
                new NestedSequenceByFunctionSequenceItem(_gridHelper.performAnimateInGrid(0, true)),
                new ConditionSequenceItem(_resultHelper.hasWins(), new NestedSequenceByFunctionSequenceItem(showWins(false))),
                new ConditionSequenceItem(hasFeature(), new NestedSequenceByFunctionSequenceItem(presentFeature())),
                new ConditionSequenceItem(hasFreespins(), new NestedSequenceByFunctionSequenceItem(freespins())),
                new NestedSequenceByFunctionSequenceItem(_presetHelper.defaultSpinEnd()),
                new FunctionSequenceItem(new ISequenceItemFunction() {
                    @Override
                    public void execute(Object[] objects) throws Exception {
                        System.out.println(_round.getSimulation().totalWin);
                    }
                })
        );
    }

    private ISequenceItemCondition hasFeature() {
        return (args) -> {
            GameMath.MathSpin spin = (GameMath.MathSpin) _round.getResult();
            return spin.hasFeature;
        };
    }

    private ISequenceItemCondition hasFreespins() {
        return (args) -> {
            GameMath.MathSpin spin = (GameMath.MathSpin) _round.getResult();
            return spin.triggersFreespins();
        };
    }

    private ISequenceItemSequence presentFeature() {
        return (args) -> new Sequence(
                new DelaySequenceItem(0.8f),
                new FunctionSequenceItem(_gridHelper.setGrid()),
                new DelaySequenceItem(0.5f),
                new NestedSequenceByFunctionSequenceItem(showWins(true))
        );
    }

    public ISequenceItemSequence showWins(boolean isFeatureWins) {
        return (args) -> {
            List<SlotWin> wins = _round.getResult().getWins();
            if (isFeatureWins)
                wins = ((GameMath.MathSpin) _round.getResult()).featureWins;

            //  Get the win amount by counting the wins.
            long win = 0;
            for (SlotWin w : wins) {
                win += w.amount;
            }

            Sequence s = new Sequence(
                    new FunctionSequenceItem(_payHelper.showPaylines(0, wins)),
                    new FunctionSequenceItem(_resultHelper.addFixedWin(win)),
                    new DelaySequenceItem(0.5f),
                    new FunctionSequenceItem(_payHelper.hidePaylines(0))
            );
            return s;
        };
    }

    private ISequenceItemSequence freespins() {
        return (args) -> new Sequence(
                new FunctionSequenceItem(_uiHelper.setVisibleCustomUI(0, true)),
                new FunctionSequenceItem(_fsHelper.setFreespinCount()),
                new FunctionSequenceItem(_resultHelper.addTotalWin(false)),
                new FunctionSequenceItem(_fsHelper.updateTotalWin()),
                new LabelSequenceItem("FS_START"),
                new DelaySequenceItem(0.4f),
                new FunctionSequenceItem(_resultHelper.resetWin()),
                new FunctionSequenceItem(_fsHelper.deductFreespinCount()),
                new FunctionSequenceItem(_resultHelper.setResult(ResultType.Freespin)),
                new NestedSequenceByFunctionSequenceItem(_gridHelper.performAnimateInGrid(true)),
                new ConditionSequenceItem(_resultHelper.hasWins(), new NestedSequenceByFunctionSequenceItem(showWins(false))),
                new ConditionSequenceItem(hasFeature(), new NestedSequenceByFunctionSequenceItem(presentFeature())),
                new FunctionSequenceItem(_resultHelper.addTotalWin(false)),
                new FunctionSequenceItem(_fsHelper.updateTotalWin()),

                new FunctionSequenceItem(_fsHelper.retriggerFreespinCount()),

                new FunctionSequenceItem(_fsHelper.nextFreespin()),
                new ConditionSequenceItem(_fsHelper.isLastFreespin(), null, new RepeatSequenceItem("FS_START"))
        );
    }

    protected Sequence idle() {
        return new Sequence();
    }
}
