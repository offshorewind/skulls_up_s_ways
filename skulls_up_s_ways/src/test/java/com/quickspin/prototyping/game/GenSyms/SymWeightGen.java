package com.quickspin.prototyping.game.GenSyms;

public class SymWeightGen {
    public static int[][] genWeights(int W, int[][] payOuts, int noakToScaleBy, int firstPayingSym, int lastPayingSym, int BASE, double biasPreWin, double biasPostWin, int powOfTenToRoundTo){//bias > 1 = bias to low, < 1= bias to high
        int[][] out = new int[W][lastPayingSym+1];
        double[] rawWeights = new double[lastPayingSym+1];
        double lastPay = (double) payOuts[lastPayingSym][noakToScaleBy];
        for (int sym = firstPayingSym; sym <= lastPayingSym ; sym++) {
            double w = Math.pow(lastPay/ (double) payOuts[sym][noakToScaleBy] , 1d/noakToScaleBy);
            rawWeights[sym] = w;
        }
        double sumWeight = sumArray(rawWeights);
        for (int x = 0; x < W; x++) {
            for (int sym = 0; sym < lastPayingSym + 1; sym++) {
                double bias = x < noakToScaleBy ? biasPreWin : biasPostWin;
                out[x][sym] = rawWeights[sym] > 0? (int) ((BASE * Math.pow(rawWeights[sym]/sumWeight, bias))) : 0;
            }
        }

        int roundFac = (int) Math.pow(10,powOfTenToRoundTo);
        if (roundFac > 0){
            for (int x = 0; x < W; x++) {
                for (int i = 0; i < out[x].length; i++) {
                    out[x][i] = out[x][i] / roundFac * roundFac;
                }
            }

        }
        return out;
    }

    static double sumArray(double[] arr){
        double total = 0;
        for (double i : arr) {
            total += i;
        }
        return total;
    }
}
