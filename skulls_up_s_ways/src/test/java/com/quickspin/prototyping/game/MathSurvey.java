package com.quickspin.prototyping.game;

import com.quickspin.math.statistics.data.DynamicLongArray;
import com.quickspin.math.statistics.survey.SurveyData;
import com.quickspin.math.statistics.util.XMLUtil;
import com.quickspin.testing.SlotStats;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

public class MathSurvey extends SurveyData {
    public MathSurvey(int bet, String[] symNames, String gameName) {
        super(bet, symNames, gameName);
    }

    @Override
    public void combineWith(SlotStats s) {
        super.combineWith(s);
        MathSurvey ms = (MathSurvey) s;
        nBNCounterBase.combineWith(ms.nBNCounterBase);
        nBNCounterFs.combineWith(ms.nBNCounterFs);
        nFSCounter.combineWith(ms.nFSCounter);

    }

    DynamicLongArray nBNCounterBase = new DynamicLongArray(5);
    DynamicLongArray nBNCounterFs = new DynamicLongArray(5);
    DynamicLongArray nFSCounter = new DynamicLongArray();


    public Map<String, Object> toMapPrint() {
        Map<String, Object> map = super.toMapPrint();
        map.put("nBNCounterBase", nBNCounterBase);
        map.put("nBNCounterFs", nBNCounterFs);
        map.put("nFSCounter", nFSCounter);

        return map;
    }

    public void toXML(String fileName) throws FileNotFoundException {
        XMLUtil.printXML(toMapPrint(), new PrintStream(new File(fileName)));
    }

    public MathSurvey(String fileName) throws IOException {
        super(fileName);
        Map<String, Object> source = (Map<String, Object>) XMLUtil.getObjectFromXML(fileName);
        this.nBNCounterBase = (DynamicLongArray) source.get("nBNCounterBase");
        this.nBNCounterFs = (DynamicLongArray) source.get("nBNCounterFs");
        this.nFSCounter = (DynamicLongArray) source.get("nFSCounter");

    }
}
