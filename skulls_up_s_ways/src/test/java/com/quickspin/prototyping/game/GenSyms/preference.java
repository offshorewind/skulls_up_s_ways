package com.quickspin.prototyping.game.GenSyms;

import com.quickspin.prototyping.game.data.Model;

public class preference {
    //    Added below for generating reels with preference (if needed)
    static int height = 3;

    public static final int WR = Model.WR;
    public static final int M1 = Model.M1;
    public static final int M2 = Model.M2;
    public static final int M3 = Model.M3;
    public static final int M4 = Model.M4;
    public static final int M5 = Model.M5;
    public static final int F6 = Model.F6;
    public static final int F7 = Model.F7;
    public static final int F8 = Model.F8;
    public static final int F9 = Model.F9;
//    public static final int BN = Model.BN; //12

    //    RAND per reel
    public static final int R1 = 500;
    public static final int R2 = 501;
    public static final int R3 = 502;
    public static final int R4 = 503;
    public static final int R5 = 504;

    public static final int[] MEDIUMS = new int[]{M2, M3, M4};
    public static final int[] LOWS = new int[]{M5, F6, F7, F8};
    public static final int[] BONUSES = new int[]{WR,
           R1,R2,R3,R4,R5};
    public static final int[] HIGHS = new int[]{M1};
    public static final int[] WILDS = new int[]{WR, R5};
    public static final int[] GOODS = new int[]{WR, M1, M2, M3, M4,M5};

    public static PreferenceHandy getSymGenPreferences(){
        PreferenceHandy preferences = new PreferenceHandy();

        preferences.SAME_SYMBOL_PENALTY = 5; // penalty of having any symbol appear more than once in a window
        preferences.WINDOW_SIZE = height;

//        Penalties for BN
        int[] BONUS_PENALTY = new int[BONUSES.length+2];
        System.arraycopy(BONUSES, 0, BONUS_PENALTY, 0, BONUSES.length);
        BONUS_PENALTY[BONUSES.length] = 2000;
        BONUS_PENALTY[BONUSES.length+1] = 3;

        preferences.categoryInWindow = new int[][]{ // penalty for having multiple symbols of same category appear in a window
                {  M1, WR, R5, 1000, 3},
                BONUS_PENALTY
        };
        preferences.adjacencies = new int[][][]{  // penalty for having two symbols of certain category appear next to eachother
                {HIGHS, HIGHS, {500}}, // {category 1,category 2,penalty}
                {GOODS, GOODS, {3}}, // {category 1,category 2,penalty}
                {HIGHS, MEDIUMS, {2}}, // {category 1,category 2,penalty}
                {MEDIUMS, MEDIUMS, {1}}, // {category 1,category 2,penalty}
                {LOWS, LOWS, {1}},
                {BONUSES, GOODS, {25}},
                {BONUSES, HIGHS, {1500}},
                {BONUSES, BONUSES, {5000}},
                {WILDS, BONUSES, {5000}},
        };
        preferences.fullWindow = new int[][]{   // penalty for having entire window full of a certain category
                {M5, F6, F7, F8, 2, height}  // {sym1,sym2,...penalty,window}
        };

        return preferences;
    }
}
