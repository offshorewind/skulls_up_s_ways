package com.quickspin.prototyping.game;

import com.quickspin.math.statistics.gui.components.Text;
import com.quickspin.math.statistics.survey.SurveyStatPainter;
import com.quickspin.testing.SlotStats;

import java.awt.*;

public class MathPainter extends SurveyStatPainter {
    public MathPainter() {
        super(GameMath.symNames);
    }

    @Override
    public void paint(SlotStats stats, Graphics2D[] g2d) {
        super.paint(stats, g2d);

        try {
            drawCustomTab5((MathSurvey) stats, g2d[5]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawCustomTab5(MathSurvey stats, Graphics2D g2d) {
        int summaryFontSize = (int)((double) 14 * 1.5D);
        int xStart = 25;

        Text.printFrequenciesFromArray(stats.nBNCounterBase, false, g2d, xStart, 25, summaryFontSize, "base BN Count");
        Text.printFrequenciesFromArray(stats.nBNCounterFs, false, g2d, 200, 25, summaryFontSize, "fs BN Count");
        Text.printFrequenciesFromArray(stats.nFSCounter, false, g2d, 400, 25, summaryFontSize, "total fs Count");
   }

}
