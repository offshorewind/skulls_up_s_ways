package com.quickspin.prototyping.game;

import java.io.PrintStream;

public class PrintHelper {
    public static void printGameScreen(PrintStream out, int[][] screen, String[] symNames, String s) {
        int[][] syms = transpose(screen);
        for (int[] row : syms) {
            for (int y = 0; y < row.length-1; y++) {
                out.print(symNames[row[y]]+s);
            }
            out.println(symNames[row[row.length-1]]);
        }
    }

    public static int[][] transpose(int[][] screen) {
        int[][] out = new int[screen[0].length][screen.length];
        for (int x = 0; x < screen.length; x++) {
            for (int y = 0; y < screen[0].length; y++) {
                out[y][x] = screen[x][y];
            }
        }
        return out;
    }

    public static int[][] transpose(int[][] screen, int filler) {
        int L = 0;
        for (int[] r : screen) {
            L = r.length > L ? r.length : L;
        }
        int[][] out = new int[L][screen.length];
        for (int x = 0; x < screen.length; x++) {
            for (int y = 0; y < L; y++) {
                out[y][x] = y >= screen[x].length? filler: screen[x][y];
            }
        }
        return out;
    }

    public static void print2DArray(PrintStream out, int[][] syms, String s) {
        for (int[] row : syms) {
            for (int y = 0; y < row.length-1; y++) {
                out.print(row[y]+s);
            }
            out.println(row[row.length-1]);
        }
    }
}
