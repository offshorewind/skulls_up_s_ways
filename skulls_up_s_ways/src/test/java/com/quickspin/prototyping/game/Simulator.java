package com.quickspin.prototyping.game;


import com.quickspin.math.statistics.gui.StatGraphics;
import com.quickspin.prototyping.game.data.Model;
import com.quickspin.testing.SlotStats;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Simulator {

    @Ignore
    @Test
    public void runSim() throws IOException, NoSuchAlgorithmException {

        GameMathTest test = new GameMathTest();

        test.setN(1_0_000_000L);
        test.persistToS3 = false;
        SlotStats stats = test.runSimulation();
        GameMathTest.BOHStats ms = (GameMathTest.BOHStats) stats;


    }

}
