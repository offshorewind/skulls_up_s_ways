package com.quickspin.prototyping.game.GenSyms;

import java.util.*;

/**
 * Created by claes.frisk on 2017-02-21.
 */
public class ReelBuilderHandy {


    static public List<Integer> flattenReel(List<List<Integer>> reel) {

        List<Integer> flatReel = new ArrayList<>();
        for (List<Integer> token : reel)
            flatReel.addAll(token);
        return flatReel;
    }


    public static List<List<Integer>> buildReelHandy(Random rng , List<List<Integer>> tokens, PreferenceHandy preferences) {
        List<List<Integer>> reel = new ArrayList<>();

        shuffle(rng, tokens);

        for (List<Integer> token : tokens) {
            List<Integer> optimalPositions = new ArrayList<>();
            int bestScore = Integer.MAX_VALUE;


            if (reel.isEmpty())
                reel.add(token);
            else {
                for (int pos = 0; pos < reel.size(); pos++) {
                    List<List<Integer>> nextReel = new ArrayList<>();
                    for (int i = 0; i < reel.size(); i++) {
                        if (i == pos)
                            nextReel.add(token);
                        nextReel.add(reel.get(i));
                    }
                    int score = evaluate(nextReel, preferences);

                    if (score < bestScore) {
                        bestScore = score;
                        optimalPositions = new ArrayList<>();
                        optimalPositions.add(pos);
                    } else if (score == bestScore)
                        optimalPositions.add(pos);


                }
                int optimalPosition = optimalPositions.get(rng.nextInt(optimalPositions.size()));
                reel.add(optimalPosition, token);

            }


        }

        return reel;
    }

    public static int evaluate(List<List<Integer>> reel, PreferenceHandy preferences) {

        int tokenId = 0;
        List<Integer> tmpReel = new ArrayList<>();
        List<Integer> tokenIds = new ArrayList<>();
        for (List<Integer> token: reel){
            tmpReel.addAll(token);
            for (int i = 0;i<token.size();i++)
                tokenIds.add(tokenId);
            tokenId++;
        }



        int score = 0;

        List<Set<Integer>> categorySets = new ArrayList<>();
        List<Integer> categoryPenalties = new ArrayList<>();
        List<Integer> categoryWindow = new ArrayList<>();

        List<Set<Integer>> fullSets = new ArrayList<>();
        List<Integer> fullPenalties = new ArrayList<>();
        List<Integer> fullWindow = new ArrayList<>();

        List<Set<Integer>> adjSets1 = new ArrayList<>();
        List<Set<Integer>> adjSets2 = new ArrayList<>();
        List<Integer> adjPenalties = new ArrayList<>();


        for (int i = 0; i < preferences.categoryInWindow.length; i++) {
            Set<Integer> categorySet = new HashSet<>();
            for (int j = 0; j < preferences.categoryInWindow[i].length - 2; j++)
                categorySet.add(preferences.categoryInWindow[i][j]);
            categorySets.add(categorySet);
            categoryPenalties.add(preferences.categoryInWindow[i][preferences.categoryInWindow[i].length - 2]);
            categoryWindow.add(preferences.categoryInWindow[i][preferences.categoryInWindow[i].length - 1]);
        }

        for (int i = 0; i < preferences.fullWindow.length; i++) {
            Set<Integer> fullSet = new HashSet<>();
            for (int j = 0; j < preferences.fullWindow[i].length - 2; j++)
                fullSet.add(preferences.fullWindow[i][j]);
            fullSets.add(fullSet);
            fullPenalties.add(preferences.fullWindow[i][preferences.fullWindow[i].length - 2]);
            fullWindow.add(preferences.fullWindow[i][preferences.fullWindow[i].length - 1]);
        }

        for (int i = 0; i < preferences.adjacencies.length; i++) {
            Set<Integer> adjSet1 = new HashSet<>();
            for (int j = 0; j < preferences.adjacencies[i][0].length; j++)
                adjSet1.add(preferences.adjacencies[i][0][j]);
            adjSets1.add(adjSet1);
            Set<Integer> adjSet2 = new HashSet<>();
            for (int j = 0; j < preferences.adjacencies[i][1].length; j++)
                adjSet2.add(preferences.adjacencies[i][1][j]);
            adjSets2.add(adjSet2);
            adjPenalties.add(preferences.adjacencies[i][2][0]);

        }

        // evaluate


        for (int a = 0; a < adjSets1.size(); a++) {
            for (int i = 0; i < tmpReel.size(); i++) {
                if (!tokenIds.get(i).equals(tokenIds.get((i + 1) % tokenIds.size()))) {
                    int thisSym = tmpReel.get(i);
                    int nextSym = tmpReel.get((i + 1) % tmpReel.size());

                    if (adjSets1.get(a).contains(thisSym) && adjSets2.get(a).contains(nextSym))
                        score += adjPenalties.get(a);
                    if (adjSets2.get(a).contains(thisSym) && adjSets1.get(a).contains(nextSym))
                        score += adjPenalties.get(a);
                }
            }
        }


        // Catch Desert Windows
        for (int f = 0; f < fullSets.size(); f++)
            for (int i = 0; i < tmpReel.size(); i++) {
                int symCount = 0;
                Set<Integer> tokenSet = new HashSet<>();
                for (int j = 0; j < fullWindow.get(f); j++) {
                    tokenSet.add(tokenIds.get((i + j) % tokenIds.size()));
                    if (fullSets.get(f).contains(tmpReel.get((i + j) % tmpReel.size()))) symCount++;
                }
                if (symCount >= fullWindow.get(f) && tokenSet.size()>1)
                    score += fullPenalties.get(f);
            }

        // Same Symbol in Window

        for (int i = 0; i < tmpReel.size(); i++) {
            List<int[]> perms = new ArrayList<>();

            for (int p1 = 0; p1 < preferences.WINDOW_SIZE - 1; p1++)
                for (int p2 = p1 + 1; p2 < preferences.WINDOW_SIZE; p2++)
                    perms.add(new int[]{p1, p2});

            for (int[] perm : perms) {
                int sym1 = tmpReel.get((i + perm[0]) % tmpReel.size());
                int sym2 = tmpReel.get((i + perm[1]) % tmpReel.size());

                int token1 = tokenIds.get((i + perm[0]) % tmpReel.size());
                int token2 = tokenIds.get((i + perm[1]) % tmpReel.size());
                if (sym1 == sym2 && token1 !=token2) {
                    score += preferences.SAME_SYMBOL_PENALTY;
                }
            }
        }

        // Two Special Symbol in window

        for (int c = 0; c < categorySets.size(); c++)
            for (int i = 0; i < tmpReel.size(); i++) {
                int symbolCount = 0;
                Set<Integer> tokenSet = new HashSet<>();
                for (int j = 0; j < categoryWindow.get(c); j++) {
                    if (categorySets.get(c).contains(tmpReel.get((i + j) % tmpReel.size()))) {
                        symbolCount++;
                        tokenSet.add(tokenIds.get((i + j) % tmpReel.size()));
                    }
                }
                if (symbolCount > 1 && tokenSet.size()>1)
                    score += categoryPenalties.get(c) * symbolCount;

            }


        return score;
    }

    public static List<List<Integer>> createTokens(int[][] symCounts, int[][] stacks, int[][] sequences) {
        List<List<Integer>> tokens = new ArrayList<>();

        Map<Integer, Integer> singlesToAdd = new HashMap<>();
        for (int[] symCount : symCounts)
            singlesToAdd.put(symCount[0], symCount[1]);

        if (stacks != null)
            for (int[] stack : stacks) {
                for (int i = 0; i < stack[1]; i++) {
                    List<Integer> token = new ArrayList<>();
                    for (int j = 0; j < stack[2]; j++)
                        token.add(stack[0]);
                    tokens.add(token);
                }
                if (singlesToAdd.get(stack[0]) > 0)
                    singlesToAdd.put(stack[0], Math.max(0, singlesToAdd.get(stack[0]) - stack[1] * stack[2]));
            }

        if (sequences != null)
            for (int[] sequence : sequences) {
                for (int i = 0; i < sequence[sequence.length - 1]; i++) {
                    List<Integer> token = new ArrayList<>();
                    for (int j = 0; j < sequence.length - 1; j++)
                        token.add(sequence[j]);
                    tokens.add(token);
                }
                for (int j = 0; j < sequence.length - 1; j++)
                    if (singlesToAdd.getOrDefault(sequence[j], 0) > 0)
                        singlesToAdd.put(sequence[j], Math.max(0, singlesToAdd.get(sequence[j]) - sequence[sequence.length - 1]));
            }

        for (int sym : singlesToAdd.keySet())
            for (int i = 0; i < singlesToAdd.get(sym); i++) {
                List<Integer> single = new ArrayList<>();
                single.add(sym);
                tokens.add(single);
            }


        return tokens;
    }

    public static void printReel(List<List<Integer>> reel, String[] symNames) {
        for (int i = 0; i < reel.size(); i++) {
            System.out.print(i + "\t");
            for (int j = 0; j < reel.get(i).size(); j++)
                System.out.print(symNames[reel.get(i).get(j)] + " ");
            System.out.println();
        }
    }

    public static void printFlatReel(List<Integer> reel, String[] symNames) {
        for (Integer sym : reel) System.out.println(symNames[sym]);
    }

    public static void shuffle (Random rng, List array) {
        int n = array.size();
        while (n > 1) {
            int k = rng.nextInt(n--); //decrements after using the value
            Object temp = array.get(n);
            array.set(n, array.get(k));
            array.set(k, temp);
        }
    }

}
