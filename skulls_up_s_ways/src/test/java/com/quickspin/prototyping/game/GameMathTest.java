package com.quickspin.prototyping.game;

import casinoworld.games.GameRound;
import casinoworld.games.helpers.SlotSpin;
import casinoworld.games.quickspin.UnitTestBase;
import com.quickspin.prototyping.game.data.Model;
import com.quickspin.testing.*;
import org.junit.Ignore;
import org.junit.Test;
import rng.RNG;

import java.io.*;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.function.BiFunction;
import java.util.function.Consumer;

public class GameMathTest extends UnitTestBase {

    private static long N = 2_000_000L;
    private static final int BET = Model.BASE_BET;

    public static boolean persistToS3 = false;

    public void setN(long n) {
        N = n;
    }

    public static SlotStats runSimulation() throws IOException, NoSuchAlgorithmException {

        BiFunction<RNG, GameRound, GameRound> gameCreator = (RNG rng, GameRound lastGame) ->
                new GameMath(rng, null);

        Consumer<GameRound> gamePlayer = (GameRound game) -> ((GameMath) game).playGame();

        URL classPath = Utils.whereFrom(gameCreator.apply(null, null));
        String hex = Utils.checksum(classPath);

        String fileName = GameMath.GAME_NAME
                + "_" + Model.VERSION
                + "_" + hex;

        SlotStats stats = ForkJoinSimulator.builder()
                .withN(N)
                .withBet(BET)
                .withGameCreator(gameCreator)
                .withGamePlayer(gamePlayer)
                .withStatsCreator(BOHStats::new)
                .withStatsCollector(GameMathTest::statsCollector)
                .withSymNames(GameMath.symNames)
                .withProgressFile(fileName)
                .withPersistToS3(persistToS3)
                .simulate();

        return stats;
    }

    public static void statsCollector(SlotStats slotStats, GameRound gameRound) {
        GameMath game = (GameMath) gameRound;
        BOHStats stats = (BOHStats) slotStats;
        GameMath.MathSpin base = (GameMath.MathSpin) game.baseSpin;

        stats.totalBaseWin += game.baseWin;
        stats.totalFeatureWin += game.featureWin;
        stats.totalFreespinsWin += game.fsWin;

        if (game.totalWin > stats.maxWin)
            stats.maxWin = game.totalWin;
        if (game.totalWin > 0)
            stats.anyHit++;

//        stats.buckets.addValue(game.totalWin);

        stats.allBuckets.addValue(game.totalWin);

        stats.baseOnlyBuckets.addValue(game.baseWin);
//        stats.baseHits.registerHits(game.baseSpin.getWins());

        if (base.hasFeature) {
            stats.nFeat++;
            stats.baseFeatureBuckets.addValue(game.featureWin);
        }

        if (game.freespins != null && game.freespins.size()>0) {
            stats.nFs++;
            for (SlotSpin fss : game.freespins) {
                stats.totalFsSpin++;
                GameMath.MathSpin fs = (GameMath.MathSpin) fss;
                long spinWin = fs.win;
                if (spinWin > 0) {
                    stats.fsHit++;
                }
//                stats.fsHits.registerHits(fs.getWins());
            }
            stats.allFSBuckets.addValue(game.fsWin);
            stats.nFsBuckets.addValue(game.freespins.size());
        }

//        survey stats
        MathSurvey surveyData = stats.surveyData;
        surveyData.totalData.addWin((int) game.totalWin);
        if (base.hasFeature){
            surveyData.baseFeatureData.addWin(base.featWin); //todo check
            surveyData.baseFeatureData.addHits(base.wins);
        }else {
            surveyData.baseData.addHits(base.wins);
        }

        surveyData.baseData.addWin((int) base.win);
        if (game.freespins != null && game.freespins.size() >0){
            surveyData.nFSCounter.addEntry(game.freespins.size());
            surveyData.fsData.addWin((int) game.fsWin);

            for (SlotSpin fss : game.freespins) {
                GameMath.MathSpin fs = (GameMath.MathSpin) fss;
                surveyData.singleFSData.addWin((int) (fs.win));
                surveyData.singleFSData.addHits(fs.wins);
                surveyData.nBNCounterFs.addEntry(fs.nBN);
            }
        }
        surveyData.balancesTracker.registerWin((int)game.totalWin, (int) game.totalBet);
        surveyData.nBNCounterBase.addEntry(base.nBN);
    }


    static class BOHStats extends SlotStats implements Serializable {


        static long[] nFsBins = new long[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        private final Buckets allBuckets = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});
        private final Buckets baseFeatureBuckets = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});
        private final Buckets allFSBuckets = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});


        private final Buckets baseOnlyBuckets = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});
        private final Buckets nFsBuckets = new Buckets(nFsBins);

        private final Buckets fsFeatureBuckets = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});

        private final Buckets fsMultiplierBucket = new Buckets(new long[]{1, (int) (0.5 * bet), bet, 2 * bet, 5 * bet, 10 * bet, 20 * bet, 50 * bet, 100 * bet, 200 * bet, 500 * bet, 1000 * bet});

        private long maxWin = 0;
        private long anyHit = 0;

        private final double[] payAtSpin = new double[nFsBins.length];
        private long fsHit = 0, totalFsSpin = 0;
        private double fsSpinHitrates = 0;

        MathSurvey surveyData;

        public BOHStats(int bet, String[] symNames) {
            super(bet, symNames);

//            this.baseHits = new HitMap(symNames, 6);
//            this.featHits = new HitMap(symNames, 6);
//            this.fsHits = new HitMap(symNames, 6);
            surveyData = new MathSurvey(bet,symNames, GameMath.GAME_NAME);
        }

        @Override
        public void combineWith(SlotStats s) {
            super.combineWith(s);

            BOHStats stats = (BOHStats) s;

            if (stats.maxWin > maxWin) {
                maxWin = stats.maxWin;
            }

            baseOnlyBuckets.combineWith(stats.baseOnlyBuckets);

//            baseHits.combineWith(stats.baseHits);
            anyHit += stats.anyHit;

            nFsBuckets.combineWith(stats.nFsBuckets);

            // Customized
            fsFeatureBuckets.combineWith(stats.fsFeatureBuckets);
            baseFeatureBuckets.combineWith(stats.baseFeatureBuckets);
            allBuckets.combineWith(stats.allBuckets);
            allFSBuckets.combineWith(stats.allFSBuckets);



            combineWith(payAtSpin, stats.payAtSpin);

            fsHit += stats.fsHit;
            totalFsSpin += stats.totalFsSpin;
            if (totalFsSpin > 0) {
                fsSpinHitrates = ((double) fsHit) / ((double) totalFsSpin);
            }
            fsMultiplierBucket.combineWith(stats.fsMultiplierBucket);

            surveyData.combineWith(stats.surveyData);
        }

        private void combineWith(double[] payAtSpin, double[] payAtSpin1) {
            for (int i = 0; i < payAtSpin.length; i++) {
                payAtSpin[i] += payAtSpin1[i];
            }
        }

        @Override
        public void print(PrintStream out) {
            super.print(out);

            out.println();
            out.println("--Custom Stats--");
            out.println();

            out.println("all buckets");
            allBuckets.print(out);
            out.println();

            out.println("baseFeatureBuckets");
            baseFeatureBuckets.print(out);
            out.println();

            out.println("allFSBuckets");
            allFSBuckets.print(out);
            out.println();

            out.println("maxWin:\t" + maxWin);
            out.println();

            out.println("nFsBuckets");
            nFsBuckets.print(out);
            out.println();

            out.println("fsFeatureBuckets");
            fsFeatureBuckets.print(out);
            out.println();

            out.println("baseFeatureBuckets");
            baseFeatureBuckets.print(out);
            out.println();

            out.println("baseOnlyBuckets");
            baseOnlyBuckets.print(out);
            out.println();

            out.println("fsPerSpinHitRates:\t" + fsSpinHitrates);
            out.println();

            out.println("fsMultiplierBuckets");
            fsMultiplierBucket.print(out);
            out.println();

            out.println("fsPerSpinWin");
            for (int i = 0; i < payAtSpin.length; i++) {
                out.println(i + "\t" + payAtSpin[i]);
            }
            out.println();

        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.defaultWriteObject();

        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();

        }
    }


    @Ignore("Manual Test")
    @Test
    public void fixSeedCompare() {
        FixSeedCompareMath.fixSeedCompare(random -> {
            GameMath game = new GameMath(random, null);
            game.totalBet = BET;
            game.playGame();
            return game;
        }, 10_000_000);
    }


}
