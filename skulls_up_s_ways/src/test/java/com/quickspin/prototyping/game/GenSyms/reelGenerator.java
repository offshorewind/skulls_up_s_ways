//package com.quickspin.prototyping.game.GenSyms;
//
//import com.quickspin.prototyping.game.GameMath;
//import com.quickspin.prototyping.game.GameMathTest;
//import com.quickspin.prototyping.game.PrintHelper;
//import com.quickspin.prototyping.game.Simulator;
//import com.quickspin.prototyping.game.data.Model;
//
//import java.io.PrintStream;
//import java.util.*;
//
//public class reelGenerator {
////    syms
//    public static final int WR = Model.WR;
//    public static final int M1 = Model.M1;
//    public static final int M2 = Model.M2;
//    public static final int M3 = Model.M3;
//    public static final int M4 = Model.M4;
//    public static final int F5 = Model.F5;
//    public static final int F6 = Model.F6;
//    public static final int F7 = Model.F7;
//    public static final int F8 = Model.F8;
//    public static final int BN = Model.BN; //12
//    public static final int CL = Model.CL; //16
//
//    public static final int RLOW1 = 101;
//    public static final int RLOW2 = 102;
//    public static final int RLOW3 = 103;
//    public static final int RLOW4 = 104;
//    public static final int RLOW5 = 105;
//    public static final int RLOW6 = 106;
//    public static final int RLOW7 = 107;
//    public static final int RLOW8 = 108;
//    public static final int RLOW9 = 109;
//    public static final int RLOW10 = 110;
//
//    //    RANDS for HIGH
//    public static final int RHIGH1 = 201;
//    public static final int RHIGH2 = 202;
//    public static final int RHIGH3 = 203;
//    public static final int RHIGH4 = 204;
//    public static final int RHIGH5 = 205;
//
//    //    RANDS for JP
//    public static final int RJP1 = 301;
//    public static final int RJP2 = 302;
//    public static final int RJP3 = 303;
//
//    //    RAND for CL
//    public static final int RANDCL = 400;
//
//    //    RAND per reel
//    public static final int R1 = 500;
//    public static final int R2 = 501;
//    public static final int R3 = 502;
//    public static final int R4 = 503;
//    public static final int R5 = 504;
//
//    public static final String[] symNames = GameMath.symNames;
//    public static final Map<Integer,String > SYM_MAP = SymbolSet.SYM_NAME_MAP;
//    public static final int N_PAYING_SYMS = R5 + 1;
//
//    static int seed =  778; //using seed to reproduce results
//
////    game config
//    static int W = 5;
//    static int height = 3;
//
////    static int[] MEDIUMS = new int[] {M1,M2,M3,M4};
////    static boolean[] isMedium = new boolean[symNames.length];
////    static {
////        for (int m : MEDIUMS) {
////            isMedium[m] = true;
////        }
////    }
//
//    static final int[] symPerReel = new int[] {160,160,160,160,230};
//
//    static int[][] baseSymWeights = SymWeightGen.genWeights(W, Model.PAY_OUT, 3, M1, F8, 100000, 0.65,0.55, 2);
//    static int[][] fsSymWeights = SymWeightGen.genWeights(W, Model.PAY_OUT, 3, M1, F8, 1000000, 1.7,1, 2);
//    static int[] baseBN = new int[] {10,0,11,0,16};
//    static int[] fsBN = new int[] {3,0,3,0,4};
//
//    static int[] baseWR = new int[] {6,5,3,7,5};
//    static int[] fsWR = new int[] {5,6,1,8,6};
//
//    static int[] baseCollect = new int[] {0,0,0,0,0};
//    static int[] baseRandCL = new int[] {0,0,0,0,0};
//    static int[] fsCollect = new int[] {0,0,0,0,3};
//    static int[] fsRandCL = new int[] {0,0,0,0,6};
//
//    static double baseStarve = 0.61;
//    static double fsStarve = 0.36;
//
//    static int[][] genSymCounts(int[][] weights, int[] symPerReel, int[] nBN, int[] nWR, int[] nCollect, int width, int nSym, double starve){
//        int[][] out = new int[width][nSym];
//        for (int x = 0; x < width; x++) {
//            int nToGo = symPerReel[x];
//            if (nBN!=null) {
//                out[x][BN] = nBN[x];
//                nToGo -= nBN[x];
//            }
//            if (nWR != null){
//                out[x][WR] = nWR[x];
//                nToGo -= nWR[x];
//            }
//            if (nCollect != null){
//                out[x][CL] = nCollect[x];
//                nToGo -= nCollect[x];
//            }
//
//            int[] w = weights[x].clone();
//            for (int sym = 0; sym < nSym; sym++) {
//                if (sym >= w.length) break;
//                if ( x < 3 && (sym-M1)%3 == x%3){
//                    w[sym] = (int) (w[sym] * starve);
//                }
//            }
//
//            int total = sum(w);
//            int nAdded = 0;
//            int lastSym = 0;
//            for (int sym = 0; sym < nSym; sym++) {
//                if (sym >= w.length) break;
//                int nToAdd = (int) ((double) (nToGo+5) * w[sym] / total);
//                if (nAdded > 0) lastSym = sym;
//                for (int i = 0; i < nToAdd; i++) {
//                    if (nAdded<nToGo){
//                        out[x][sym] ++;
//                        nAdded ++;
//                    }else{
//                        break;
//                    }
//                }
//            }
//            while(sum(out[x]) < symPerReel[x]){
//                out[x][lastSym] ++;
//            }
//        }
//        return out;
//    }
//
//    public static void main(String[] args) throws Exception {
//
////        //base
////        genBase(888);
//
////        //fs
//        genFs(888);
//    }
//
//    static void genBase(int seed) throws Exception {
//        Map<Integer, int[][]> map = new HashMap<>();
//        int[] symToAdd = symPerReel.clone();
//        map.put(1, genSymCounts(baseSymWeights, symToAdd, baseBN, baseWR, baseCollect, 5, N_PAYING_SYMS, baseStarve));
//        List<List<int[]>> customSyms = new ArrayList<>();
//        for (int x = 0; x < W; x++) {
//            customSyms.add(new ArrayList<>());
//        }
//        customSyms.get(0).add(new int[]{RLOW1,RLOW3, 1});
//        customSyms.get(0).add(new int[]{RHIGH1,RLOW4, 1});
//        customSyms.get(0).add(new int[]{RHIGH3,RLOW2, 1});
//        for (int R = RLOW6; R <= RLOW10; R++) {
//            customSyms.get(0).add(new int[]{ R, 1});
//        }
//        for (int R = RHIGH2; R <= RHIGH5; R++) {
//            customSyms.get(0).add(new int[]{ R, 1});
//        }
////        customSyms.get(0).add(new int[]{CNHIGH,CNLOW,CNLOW, 1});
//
//        customSyms.get(1).add(new int[]{RLOW4,RLOW1, 1});
////        customSyms.get(1).add(new int[]{RLOW3,RLOW2, 1});
//        for (int R = RHIGH1; R <= RHIGH5; R++) {
//            customSyms.get(1).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW7; R <= RLOW10; R++) {
//            customSyms.get(1).add(new int[]{ R, 1});
//        }
//        customSyms.get(1).add(new int[]{RHIGH4,RLOW2,RLOW1, 1});
//
//        customSyms.get(2).add(new int[]{RHIGH5,RLOW1,2});
//        customSyms.get(2).add(new int[]{RLOW3,RLOW2,1});
//        customSyms.get(2).add(new int[]{RLOW3, RHIGH2,1});
//        for (int R = RHIGH2; R <= RHIGH5; R++) {
//            customSyms.get(2).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW6; R <= RLOW10; R++) {
//            customSyms.get(2).add(new int[]{ R, 1});
//        }
//        customSyms.get(2).add(new int[]{RLOW3,RHIGH5,RLOW1, 1});
//
//        for (int R = RHIGH3; R <= RHIGH5; R++) {
//            customSyms.get(3).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW6; R <= RLOW10; R++) {
//            customSyms.get(3).add(new int[]{ R, 1});
//        }
//        for (int i = 0; i < 3; i++) {
//            customSyms.get(3).add(new int[]{RLOW1+i,RLOW6-i,1});
//        }
//        customSyms.get(3).add(new int[]{RHIGH3, RLOW2, 1});
//        customSyms.get(3).add(new int[]{RHIGH4, RHIGH5, 1});
//        customSyms.get(3).add(new int[]{RHIGH4, RLOW9, 1});
//        customSyms.get(3).add(new int[]{RLOW4,RLOW7,RHIGH2, 1});
//
//        customSyms.get(1).add(new int[]{WR,WR, 1});
//        customSyms.get(2).add(new int[]{WR,WR,WR, 2});
//        customSyms.get(3).add(new int[]{WR,WR, 1});
//
//        for (int i = 0; i < W; i++) {
//            customSyms.get(i).add(new int[] {M1,M1,1});
//        }
//
//        customSyms.get(2).add(new int[]{RJP1,1});
////        customSyms.get(3).add(new int[]{RJP1,1});
//        customSyms.get(3).add(new int[]{RJP1,RJP2,1});
//        customSyms.get(3).add(new int[]{RJP3,1});
//
//        customSyms.get(0).add(new int[]{R1,4});
//        customSyms.get(0).add(new int[]{R1,R2,1});
//        customSyms.get(1).add(new int[]{R2,R3,1});
//        customSyms.get(1).add(new int[]{R2,4});
//        customSyms.get(2).add(new int[]{R3,R2,1});
//        customSyms.get(2).add(new int[]{R3,5});
//        customSyms.get(3).add(new int[]{R4,5});
//        customSyms.get(3).add(new int[]{R4,R3,1});
//        customSyms.get(3).add(new int[]{R4,R3,1});
//        customSyms.get(4).add(new int[]{R5,5});
//
//        int[][] baseGrid = genWithPreference(new Random(seed), map, preference.getSymGenPreferences(), customSyms, N_PAYING_SYMS, W);
//        GameMath.reelContainer.set(0, baseGrid);
//        GameMathTest.runSimulation();
//        symPrinter(System.out, baseGrid, "BaseGameGrids", SYM_MAP );
//    }
//
//    static void genFs(int seed) throws Exception {
//        Map<Integer, int[][]> map = new HashMap<>();
//
//        //        //fs
//        int[] symToAdd = symPerReel.clone();
//        map.put(1, genSymCounts(fsSymWeights, symToAdd, fsBN, fsWR, fsCollect, 5, N_PAYING_SYMS, fsStarve));
//        List<List<int[]>> customSyms = new ArrayList<>();
//        for (int x = 0; x < W; x++) {
//            customSyms.add(new ArrayList<>());
//        }
//        customSyms.get(0).add(new int[]{RLOW1,RLOW2, 1});
//        customSyms.get(0).add(new int[]{RLOW3,RLOW2, 1});
//        customSyms.get(0).add(new int[]{RLOW2,RLOW5, 1});
////        customSyms.get(0).add(new int[]{RHIGH3,1});
//        customSyms.get(0).add(new int[]{RLOW5,RLOW10, 1});
//        for (int R = RLOW4; R <= RLOW9; R++) {
//            customSyms.get(0).add(new int[]{ R, 1});
//        }
//
////        customSyms.get(1).add(new int[]{RLOW3,RLOW1, 1});
//        customSyms.get(1).add(new int[]{RLOW2,RLOW3, 1});
//        for (int R = RHIGH2; R <= RHIGH4; R++) {
//            customSyms.get(1).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW3; R <= RLOW9; R++) {
//            customSyms.get(1).add(new int[]{ R, 1});
//        }
//        customSyms.get(1).add(new int[]{RHIGH1,RLOW3,RLOW1, 1});
//
//        customSyms.get(2).add(new int[]{RLOW5,RLOW1,1});
//        customSyms.get(2).add(new int[]{RLOW2,RLOW4,1});
//        for (int R = RHIGH4; R <= RHIGH5; R++) {
//            customSyms.get(2).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW7; R <= RLOW9; R++) {
//            customSyms.get(2).add(new int[]{ R, 1});
//        }
//        customSyms.get(2).add(new int[]{RLOW4,RLOW2,RLOW6, 1});
//
//
//        for (int R = RHIGH4; R <= RHIGH4; R++) {
//            customSyms.get(3).add(new int[]{ R, 1});
//        }
//        for (int R = RLOW6; R <= RLOW8; R++) {
//            customSyms.get(3).add(new int[]{ R, 1});
//        }
//        for (int i = 0; i < 1; i++) {
//            customSyms.get(3).add(new int[]{RLOW1+i,RLOW6-i,1});
//        }
//        customSyms.get(3).add(new int[]{RLOW4,RLOW7,RHIGH2, 1});
//
//        customSyms.get(2).add(new int[]{WR,WR,WR,2});
//
//        customSyms.get(2).add(new int[]{RJP1,1});
//        customSyms.get(3).add(new int[]{RJP2,1});
//        customSyms.get(3).add(new int[]{RJP3,1});
//
//        customSyms.get(4).add(new int[] {RANDCL, fsRandCL[4]});
//
//        customSyms.get(0).add(new int[]{R1,4});
//        customSyms.get(1).add(new int[]{R2,3});
//        customSyms.get(2).add(new int[]{R3,R3,2});
//        customSyms.get(3).add(new int[]{R4,5});
//        customSyms.get(4).add(new int[]{R5,4});
//
//        for (int i = 0; i < W; i++) {
//            customSyms.get(i).add(new int[] {M1,M1,1});
//        }
//
//        int[][] fsGrid = genWithPreference(new Random(seed), map, preference.getSymGenPreferences(), customSyms, N_PAYING_SYMS, W);
//        GameMath.reelContainer.set(1, fsGrid);
//        GameMathTest.runSimulation();
////        printXls(fsGrid, SYM_MAP, "\t\t\t");
//        symPrinter(System.out, fsGrid, "FreeGameGrids", SYM_MAP);
//    }
//
//    public static void symPrinter(PrintStream out, int[][] reels, String gridName, String[] symNames){
//        String start = "public static final int[][] *Grid* = {";
//        start = start.replace("*Grid*", gridName);
//        out.println(start);
//        for (int[] reel : reels) {
//            out.print("{");
//            for (int sym : reel) {
//                out.print(symNames[sym] + ",");
//            }
//            out.println("},");
//        }
//        out.println("};");
//    }
//
//    public static void symPrinter(PrintStream out, int[][] reels, String gridName, Map<Integer,String> symNamesMap){
//        String start = "public static final int[][] *Grid* = {";
//        start = start.replace("*Grid*", gridName);
//        out.println(start);
//        for (int[] reel : reels) {
//            out.print("{");
//            for (int sym : reel) {
//                out.print(symNamesMap.get(sym) + ",");
//            }
//            out.println("},");
//        }
//        out.println("};");
//    }
//
//    public static void printXls(int[][] reel, String[] symNames, String sep){
//        int[][] toPrint = PrintHelper.transpose(reel,0);
//        for (int row = 0; row < toPrint.length; row++) {
//            int L = toPrint[row].length;
//            for (int col = 0; col < L - 1; col++) {
//                int sym = toPrint[row][col];
//                System.out.print(symNames[sym]+sep);
//            }
//            System.out.println(symNames[toPrint[row][L-1]]);
//        }
//    }
//
//    public static void printXls(int[][] reel, Map<Integer,String> SYM_MAP, String sep){
//        int[][] toPrint = PrintHelper.transpose(reel,0);
//        for (int row = 0; row < toPrint.length; row++) {
//            int L = toPrint[row].length;
//            for (int col = 0; col < L - 1; col++) {
//                int sym = toPrint[row][col];
//                System.out.print(SYM_MAP.get(sym)+sep);
//            }
//            System.out.println(SYM_MAP.get(toPrint[row][L-1]));
//        }
//    }
//
//    static double sum(double[] array){
//        double out = 0;
//        for (double i :
//                array) {
//            out += i;
//        }
//        return out;
//    }
//
//    static int sum(int[] array){
//        int out = 0;
//        for (int i :
//                array) {
//            out += i;
//        }
//        return out;
//    }
//
//    public static int[][] genWithPreference(Random rng, Map<Integer, int[][]> symCount, PreferenceHandy preferences, List<List<int[]>> customSyms, int nUniqueSym, int width){
//        int[][] out = new int[width][];
//
//        int[][][] symCountArray = new int[width][nUniqueSym][];
//        int[][] singleCount = symCount.get(1);
//        for (int x = 0; x < width; x++) {
//            for (int sym = 0; sym < nUniqueSym; sym++) {
//                symCountArray[x][sym] = new int[] {sym, singleCount[x][sym]};
//            }
//        }
//
//        int[][][] stacksArray = new int[width][][];
//        for (int x = 0; x < width; x++) {
//            List<int[]> validCounts = new ArrayList<>();
//            for (int length :symCount.keySet()) {
//                if(length > 1){
//                    int[][] stackCounts = symCount.get(length);
//                    for (int sym = 0; sym < nUniqueSym; sym++) {
//                        int count = stackCounts[x][sym];
//                        if ( count > 0 ){
//                            validCounts.add(new int[]{sym, count, length});
//                            symCountArray[x][sym][1] += count * length;
//                        }
//                    }
//                }
//            }
//
//            stacksArray[x] = new int[validCounts.size()][];
//            for (int i = 0; i < stacksArray[x].length; i++) {
//                stacksArray[x][i] = validCounts.get(i).clone();
//            }
//        }
//
//        int[][][] sequences = new int[width][][];
//        boolean hasCustomSyms = false;
//        if (customSyms != null && !customSyms.isEmpty()){
//            for (List<int[]> reel: customSyms) {
//                if (!reel.isEmpty()){
//                    hasCustomSyms = true;
//                    break;
//                }
//            }
//        }
//        if (hasCustomSyms){
//            for (int x = 0; x < width; x++) {
//                List<int[]> reelCustomSyms = customSyms.get(x);
//                if (reelCustomSyms.size() >0){
//                    int[][] seqArray = new int[reelCustomSyms.size()][];
//                    for (int i = 0; i < seqArray.length; i++) {
//                        seqArray[i] = reelCustomSyms.get(i).clone();
//                        for (int j = 0; j < seqArray[i].length - 1; j++) {
//                            int sym = seqArray[i][j];
//                            symCountArray[x][sym][1] += seqArray[i][seqArray[i].length-1];
//                        }
//                    }
//                    sequences[x] = seqArray;
//                }else{
//                    sequences[x] = null;
//                }
//            }
//        }
//
//        for (int x = 0; x < width; x++) {
//            List<List<Integer>> tokens = ReelBuilderHandy.createTokens(symCountArray[x], stacksArray[x], sequences[x]);
//            List<List<Integer>> reel = ReelBuilderHandy.buildReelHandy(rng, tokens, preferences);
//            out[x] = ReelBuilderHandy.flattenReel(reel).stream().mapToInt( o-> o.intValue()).toArray();
//        }
//
//        return out;
//    }
//
//}
