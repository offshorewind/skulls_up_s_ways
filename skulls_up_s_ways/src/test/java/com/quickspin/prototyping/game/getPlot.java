package com.quickspin.prototyping.game;

import com.quickspin.math.statistics.gui.StatGraphics;
import com.quickspin.prototyping.game.data.Model;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class getPlot {
    @Ignore
    @Test
    void name() throws IOException {
//        MidasSurvey ms = new MidasSurvey("MidasCoins_0.3.2_HashMap.xml");
        MathSurvey ms = new MathSurvey(Model.VERSION+"_HashMap.xml");
        StatGraphics sg = new StatGraphics(Model.VERSION, new String[] {"Summary","Base","Fs","KPI","Broke","Custom"}, 1920, 900, new MathPainter());
        ms.plotGraph(sg);
    }
}
