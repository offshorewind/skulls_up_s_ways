package com.quickspin.prototyping.game.GenSyms;

import java.util.HashMap;
import java.util.Map;

public class SymbolSet {
    public static final int WR = 1;
    public static final int M1 = 2;
    public static final int M2 = 3;
    public static final int M3 = 4;
    public static final int M4 = 5;
    public static final int F5 = 6;
    public static final int F6 = 7;
    public static final int F7 = 8;
    public static final int F8 = 9;
    public static final int BN = 10;
    public static final int CL = 11;
    // LOW COIN SYMBOLS
    public static final int X1 = 12;
    public static final int X2 = 13;
    public static final int X3 = 14;
    public static final int X4 = 15;
    public static final int X5 = 16;
    // HIGH COIN SYMBOLS
    public static final int X7 = 17;
    public static final int X10 = 18;
    public static final int X15 = 19;
    public static final int X20 = 20;
    // JP COIN SYMBOLS
    public static final int X30 = 21;
    public static final int X50 = 22;
    public static final int X100 = 23;
    public static final int X200 = 24;
    public static final int X500 = 25;

    //    RANDS for LOW
    public static final int RLOW1 = 101;
    public static final int RLOW2 = 102;
    public static final int RLOW3 = 103;
    public static final int RLOW4 = 104;
    public static final int RLOW5 = 105;
    public static final int RLOW6 = 106;
    public static final int RLOW7 = 107;
    public static final int RLOW8 = 108;
    public static final int RLOW9 = 109;
    public static final int RLOW10 = 110;

    //    RANDS for HIGH
    public static final int RHIGH1 = 201;
    public static final int RHIGH2 = 202;
    public static final int RHIGH3 = 203;
    public static final int RHIGH4 = 204;
    public static final int RHIGH5 = 205;

    //    RANDS for JP
    public static final int RJP1 = 301;
    public static final int RJP2 = 302;
    public static final int RJP3 = 303;

    //    RAND for CL
    public static final int RANDCL = 400;

    //    RAND per reel
    public static final int R1 = 500;
    public static final int R2 = 501;
    public static final int R3 = 502;
    public static final int R4 = 503;
    public static final int R5 = 504;

    public static final Map<Integer,String> SYM_NAME_MAP = new HashMap<>();
    static {
        SYM_NAME_MAP.put(0, "  ");

        SYM_NAME_MAP.put(WR, "WR");
        SYM_NAME_MAP.put(M1, "M1");
        SYM_NAME_MAP.put(M2, "M2");
        SYM_NAME_MAP.put(M3, "M3");
        SYM_NAME_MAP.put(M4, "M4");
        SYM_NAME_MAP.put(F5, "F5");
        SYM_NAME_MAP.put(F6, "F6");
        SYM_NAME_MAP.put(F7, "F7");
        SYM_NAME_MAP.put(F8, "F8");
        SYM_NAME_MAP.put(BN, "BN");
        SYM_NAME_MAP.put(CL, "CL");

        SYM_NAME_MAP.put(RLOW1, "RLOW1");
        SYM_NAME_MAP.put(RLOW2, "RLOW2");
        SYM_NAME_MAP.put(RLOW3, "RLOW3");
        SYM_NAME_MAP.put(RLOW4, "RLOW4");
        SYM_NAME_MAP.put(RLOW5, "RLOW5");
        SYM_NAME_MAP.put(RLOW6, "RLOW6");
        SYM_NAME_MAP.put(RLOW7, "RLOW7");
        SYM_NAME_MAP.put(RLOW8, "RLOW8");
        SYM_NAME_MAP.put(RLOW9, "RLOW9");
        SYM_NAME_MAP.put(RLOW10, "RLOW10");

        SYM_NAME_MAP.put(RHIGH1, "RHIGH1");
        SYM_NAME_MAP.put(RHIGH2, "RHIGH2");
        SYM_NAME_MAP.put(RHIGH3, "RHIGH3");
        SYM_NAME_MAP.put(RHIGH4, "RHIGH4");
        SYM_NAME_MAP.put(RHIGH5, "RHIGH5");

        SYM_NAME_MAP.put(RJP1, "RJP1");
        SYM_NAME_MAP.put(RJP2, "RJP2");
        SYM_NAME_MAP.put(RJP3, "RJP3");

        SYM_NAME_MAP.put(RANDCL, "RANDCL");

        SYM_NAME_MAP.put(R1, "R1");
        SYM_NAME_MAP.put(R2, "R2");
        SYM_NAME_MAP.put(R3, "R3");
        SYM_NAME_MAP.put(R4, "R4");
        SYM_NAME_MAP.put(R5, "R5");
    }
}
